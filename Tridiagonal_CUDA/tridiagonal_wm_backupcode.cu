
template<typename val_t>
__global__ void WM6_kernel(val_t *a_gbl, val_t *b_gbl, val_t *c_gbl, val_t *d_gbl, int n_eqt, int n_batch) {
    const unsigned int shuffle_mask = 0xffffffff;
    const int warp_size = 32;
    val_t *a_mem = a_gbl + blockIdx.x*n_eqt;
    val_t *b_mem = b_gbl + blockIdx.x*n_eqt;
    val_t *c_mem = c_gbl + blockIdx.x*n_eqt;
    val_t *d_mem = d_gbl + blockIdx.x*n_eqt;
    __shared__ val_t c_shared[64], last_c[1], c_bcast[2];
    __shared__ val_t d_shared[64], last_d[1], d_bcast[2];
    __shared__ val_t a_shared[64], a_bcast[2];
    __shared__ val_t b_shared[64], b_bcast[2];
    
    if(threadIdx.x == 0) {
        last_c[0] = ZERO(val_t); last_d[0] = ZERO(val_t);
    }
    for(int i = 0; i < n_eqt; i += 2 * 64) {
        val_t al, ah, bl, bh, cl, ch, dl, dh;
        auto a2 = load_2(a_mem + i + threadIdx.x * 2);
        auto b2 = load_2(b_mem + i + threadIdx.x * 2);
        auto c2 = load_2(c_mem + i + threadIdx.x * 2);
        auto d2 = load_2(d_mem + i + threadIdx.x * 2);
        al = a2.x; bl = b2.x; cl = c2.x; dl = d2.x;
        ah = a2.y; bh = b2.y; ch = c2.y; dh = d2.y;
        {
            int tidf, tidl, pred, loc;
            val_t k1, k2, k3, e, all, ahf, bll, bhf, cll, chf, dll, dhf, at, bt, ct, dt;

            k1 = div(ah, - bl);
            bh = bh + k1 * cl;
            ah =      k1 * al;
            dh = dh + k1 * dl;
            k2 = div(cl, - bh);
            al = al + k2 * ah;
            cl =      k2 * ch;
            dl = dl + k2 * dh;
    
            pred = threadIdx.x & 1;
            at = shfl_xor((pred == 0) ? ah : al, 1);
            bt = shfl_xor((pred == 0) ? bh : bl, 1);
            ct = shfl_xor((pred == 0) ? ch : cl, 1);
            dt = shfl_xor((pred == 0) ? dh : dl, 1);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
    
            tidf = threadIdx.x & (~1);
            tidl = threadIdx.x | 1;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((threadIdx.x == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (threadIdx.x == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(threadIdx.x == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = threadIdx.x & 2;
            at = shfl_xor((pred == 0) ? ah : al, 2);
            bt = shfl_xor((pred == 0) ? bh : bl, 2);
            ct = shfl_xor((pred == 0) ? ch : cl, 2);
            dt = shfl_xor((pred == 0) ? dh : dl, 2);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            tidf = threadIdx.x & (~3);
            tidl = threadIdx.x | 3;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((threadIdx.x == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (threadIdx.x == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(threadIdx.x == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = threadIdx.x & 4;
            at = shfl_xor((pred == 0) ? ah : al, 4);
            bt = shfl_xor((pred == 0) ? bh : bl, 4);
            ct = shfl_xor((pred == 0) ? ch : cl, 4);
            dt = shfl_xor((pred == 0) ? dh : dl, 4);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            tidf = threadIdx.x & (~7);
            tidl = threadIdx.x | 7;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((threadIdx.x == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (threadIdx.x == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(threadIdx.x == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = threadIdx.x & 8;
            at = shfl_xor((pred == 0) ? ah : al, 8);
            bt = shfl_xor((pred == 0) ? bh : bl, 8);
            ct = shfl_xor((pred == 0) ? ch : cl, 8);
            dt = shfl_xor((pred == 0) ? dh : dl, 8);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            tidf = threadIdx.x & (~15);
            tidl = threadIdx.x | 15;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((threadIdx.x == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (threadIdx.x == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(threadIdx.x == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = threadIdx.x & 16;
            at = shfl_xor((pred == 0) ? ah : al, 16);
            bt = shfl_xor((pred == 0) ? bh : bl, 16);
            ct = shfl_xor((pred == 0) ? ch : cl, 16);
            dt = shfl_xor((pred == 0) ? dh : dl, 16);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }

            /* ROUND 6 */
            tidf = threadIdx.x & (~31);
            tidl = threadIdx.x | 31;

            loc = threadIdx.x >> 5;
            if(threadIdx.x == tidl) {
                a_shared[loc] = al; b_shared[loc] = bl; c_shared[loc] = cl; d_shared[loc] = dl;
            }
            __syncthreads();
            all = a_shared[loc];
            bll = b_shared[loc];
            cll = c_shared[loc];
            dll = d_shared[loc];
            k1 = div(ah, - bll);
            e = ((threadIdx.x == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (threadIdx.x == tidf) ? e : bh;

            if(threadIdx.x == tidf) {
                a_bcast[loc] = ah; b_bcast[loc] = bh; c_bcast[loc] = ch; d_bcast[loc] = dh;
            }
            __syncthreads();
            ahf = a_bcast[loc];
            bhf = b_bcast[loc];
            chf = c_bcast[loc];
            dhf = d_bcast[loc];
            k2 = div(cl, - bhf);
            k3 = maskz_div(threadIdx.x == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;

            pred = threadIdx.x & 32;
            loc = threadIdx.x ^ 32;
            a_shared[threadIdx.x] = (pred == 0) ? ah : al;
            b_shared[threadIdx.x] = (pred == 0) ? bh : bl;
            c_shared[threadIdx.x] = (pred == 0) ? ch : cl;
            d_shared[threadIdx.x] = (pred == 0) ? dh : dl;
            __syncthreads();
            if(pred == 0) {
                ah = a_shared[loc], bh = b_shared[loc], ch = c_shared[loc], dh = d_shared[loc];
            } else {
                al = a_shared[loc], bl = b_shared[loc], cl = c_shared[loc], dl = d_shared[loc];
            }

            /* pGE */
            k1 = - al;
            e = ((threadIdx.x == 0) ? bl : ZERO(val_t)) + k1 * last_c[0];
            dl = dl + k1 * last_d[0];
            bl = (threadIdx.x == 0) ? e : bl;
            if(threadIdx.x == 0) {
                b_bcast[0] = e; c_bcast[0] = cl; d_bcast[0] = dl;
            }
            __syncthreads();
            bhf = b_bcast[0];
            chf = c_bcast[0];
            dhf = d_bcast[0];
            k3 = maskz_div(threadIdx.x == 0, e, - bhf);
            cl = div(cl + k3 * chf, bl);
            dl = div(dl + k3 * dhf, bl);
            c_mem[threadIdx.x + i] = cl;
            d_mem[threadIdx.x + i] = dl;
            if(threadIdx.x == 63) {
                last_c[0] = cl; last_d[0] = dl;
            }
            __syncthreads();
    
            k1 = - ah;
            e = ((threadIdx.x == 0) ? bh : ZERO(val_t)) + k1 * last_c[0];
            dh = dh + k1 * last_d[0];
            bh = (threadIdx.x == 0) ? e : bh;
            if(threadIdx.x == 0) {
                b_bcast[0] = e; c_bcast[0] = ch; d_bcast[0] = dh;
            }
            __syncthreads();
            bhf = b_bcast[0];
            chf = c_bcast[0];
            dhf = d_bcast[0];
            k3 = maskz_div(threadIdx.x == 0, e, - bhf);
            ch = div(ch + k3 * chf, bh);
            dh = div(dh + k3 * dhf, bh);
            c_mem[i + (threadIdx.x | 64)] = ch;
            d_mem[i + (threadIdx.x | 64)] = dh;
            if(threadIdx.x == 63) {
                last_c[0] = ch; last_d[0] = dh;
            }
        }
    }
    __syncthreads();
    if(threadIdx.x == 0) {
        last_d[0] = d_mem[n_eqt - 64];
    }
    __syncthreads();
    for(int i = n_eqt-2*64; i >= 0; i -= 64) {
        val_t x = d_mem[i+threadIdx.x] - last_d[0] * c_mem[i+threadIdx.x];
        d_mem[i+threadIdx.x] = x;
        if(threadIdx.x == 0) {
            last_d[0] = x;
        }
        __syncthreads();
    }
}

template<typename val_t, int BLOCK_FUSION>
__global__ void real_WM4_reg_kernel(val_t *a_gbl, val_t *b_gbl, val_t *c_gbl, val_t *d_gbl, int n_eqt, int n_batch) {
    const unsigned int shuffle_mask = 0xffffffff;
    const int warp_size = 16;
    const int tid = threadIdx.x & 15;
    const int bid = (blockIdx.x * (BLOCK_FUSION * warp_size) + threadIdx.x) >> 4;
    if(bid >= n_batch) { return; }
    val_t *a_mem = a_gbl + bid*n_eqt;
    val_t *b_mem = b_gbl + bid*n_eqt;
    val_t *c_mem = c_gbl + bid*n_eqt;
    val_t *d_mem = d_gbl + bid*n_eqt;
    
    val_t last_c = ZERO(val_t), last_d = ZERO(val_t);
    for(int i = 0; i < n_eqt; i += 2 * warp_size) {
        val_t al, ah, bl, bh, cl, ch, dl, dh;
        auto a2 = load_2(a_mem + i + tid * 2);
        auto b2 = load_2(b_mem + i + tid * 2);
        auto c2 = load_2(c_mem + i + tid * 2);
        auto d2 = load_2(d_mem + i + tid * 2);
        al = a2.x; bl = b2.x; cl = c2.x; dl = d2.x;
        ah = a2.y; bh = b2.y; ch = c2.y; dh = d2.y;
        {
            int tidf, tidl, pred;
            val_t k1, k2, k3, e, all, ahf, bll, bhf, cll, chf, dll, dhf, at, bt, ct, dt;

            k1 = div(ah, - bl);
            bh = bh + k1 * cl;
            ah =      k1 * al;
            dh = dh + k1 * dl;
            k2 = div(cl, - bh);
            al = al + k2 * ah;
            cl =      k2 * ch;
            dl = dl + k2 * dh;
    
            pred = tid & 1;
            at = shfl_xor((pred == 0) ? ah : al, 1);
            bt = shfl_xor((pred == 0) ? bh : bl, 1);
            ct = shfl_xor((pred == 0) ? ch : cl, 1);
            dt = shfl_xor((pred == 0) ? dh : dl, 1);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            /* ROUND 2-5 */
            /*
            #pragma unroll
            for(int bit = 2; bit < 32; bit = bit << 1) {
                
            }*/
    
            tidf = tid & (~1);
            tidl = tid | 1;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((tid == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (tid == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(tid == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = tid & 2;
            at = shfl_xor((pred == 0) ? ah : al, 2);
            bt = shfl_xor((pred == 0) ? bh : bl, 2);
            ct = shfl_xor((pred == 0) ? ch : cl, 2);
            dt = shfl_xor((pred == 0) ? dh : dl, 2);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            tidf = tid & (~3);
            tidl = tid | 3;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((tid == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (tid == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(tid == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = tid & 4;
            at = shfl_xor((pred == 0) ? ah : al, 4);
            bt = shfl_xor((pred == 0) ? bh : bl, 4);
            ct = shfl_xor((pred == 0) ? ch : cl, 4);
            dt = shfl_xor((pred == 0) ? dh : dl, 4);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }
            
            tidf = tid & (~7);
            tidl = tid | 7;
            all = shfl(al, tidl);
            bll = shfl(bl, tidl);
            cll = shfl(cl, tidl);
            dll = shfl(dl, tidl);
            k1 = div(ah, - bll);
            e = ((tid == tidf) ? bh : ZERO(val_t)) + k1 * cll;
            ah =      k1 * all;
            dh = dh + k1 * dll;
            bh = (tid == tidf) ? e : bh;
            ahf = shfl(ah, tidf);
            bhf = shfl(e, tidf);
            chf = shfl(ch, tidf);
            dhf = shfl(dh, tidf);
            k2 = div(cl, - bhf);
            k3 = maskz_div(tid == tidf, e, - bhf);
            al = al + k2 * ahf;
            cl =      k2 * chf;
            dl = dl + k2 * dhf;
            ah = ah + k3 * ahf;
            ch = ch + k3 * chf;
            dh = dh + k3 * dhf;
                
            pred = tid & 8;
            at = shfl_xor((pred == 0) ? ah : al, 8);
            bt = shfl_xor((pred == 0) ? bh : bl, 8);
            ct = shfl_xor((pred == 0) ? ch : cl, 8);
            dt = shfl_xor((pred == 0) ? dh : dl, 8);
            if(pred == 0) {
                ah = at, bh = bt, ch = ct, dh = dt;
            } else {
                al = at, bl = bt, cl = ct, dl = dt;
            }

            k1 = - al;
            e = ((tid == 0) ? bl : ZERO(val_t)) + k1 * last_c;
            dl = dl + k1 * last_d;
            bl = (tid == 0) ? e : bl;
            bhf = shfl(e, 0);
            chf = shfl(cl, 0);
            dhf = shfl(dl, 0);
            k3 = maskz_div(tid == 0, e, - bhf);
            cl = div(cl + k3 * chf, bl);
            dl = div(dl + k3 * dhf, bl);
            c_mem[tid + i] = cl;
            d_mem[tid + i] = dl;
            last_c = shfl(cl, 15);
            last_d = shfl(dl, 15);
    
            k1 = - ah;
            e = ((tid == 0) ? bh : ZERO(val_t)) + k1 * last_c;
            dh = dh + k1 * last_d;
            bh = (tid == 0) ? e : bh;
            bhf = shfl(e, 0);
            chf = shfl(ch, 0);
            dhf = shfl(dh, 0);
            k3 = maskz_div(tid == 0, e, - bhf);
            ch = div(ch + k3 * chf, bh);
            dh = div(dh + k3 * dhf, bh);
            c_mem[i + (tid | 16)] = ch;
            d_mem[i + (tid | 16)] = dh;
            last_c = shfl(ch, 15);
            last_d = shfl(dh, 15);
        }
    }
    val_t last_x = d_mem[n_eqt - warp_size];
    for(int i = n_eqt-2*warp_size; i >= 0; i -= warp_size) {
        val_t x = d_mem[i+tid] - last_x * c_mem[i+tid];
        d_mem[i+tid] = x;
        last_x = shfl(x, 0);
    }
}


template<typename val_t, int BLOCK_FUSION>
__global__ void WM5_R4_reg_kernel(val_t *a_gbl, val_t *b_gbl, val_t *c_gbl, val_t *d_gbl, int n_eqt, int n_batch) {
    const unsigned int shuffle_mask = 0xffffffff;
    const int warp_size = warpSize;
    const int tid = threadIdx.x & 31;
    const int bid = (blockIdx.x * (BLOCK_FUSION * WARP_SIZE) + threadIdx.x) >> 5;
    if(bid >= n_batch) { return; }
    val_t *a_mem = a_gbl + bid*n_eqt;
    val_t *b_mem = b_gbl + bid*n_eqt;
    val_t *c_mem = c_gbl + bid*n_eqt;
    val_t *d_mem = d_gbl + bid*n_eqt;
    
    val_t last_c = ZERO(val_t), last_d = ZERO(val_t);
    for(int i = 0; i < n_eqt; i += 4 * WARP_SIZE) {
        val_t ax, ay, az, aw, bx, by, bz, bw, cx, cy, cz, cw, dx, dy, dz, dw;
        auto a4 = load_4(a_mem + i + tid * 4);
        auto b4 = load_4(b_mem + i + tid * 4);
        auto c4 = load_4(c_mem + i + tid * 4);
        auto d4 = load_4(d_mem + i + tid * 4);
        ax = a4.x; bx = b4.x; cx = c4.x; dx = d4.x;
        ay = a4.y; by = b4.y; cy = c4.y; dy = d4.y;
        az = a4.z; bz = b4.z; cz = c4.z; dz = d4.z;
        aw = a4.w; bw = b4.w; cw = c4.w; dw = d4.w;
        {
            int tidf, tidl, pred;
            val_t k1, k2, k3, k4, k5, k6, e1, e2, at0, at1, bt0, bt1, ct0, ct1, dt0, dt1;
            val_t al, af, bl, bf, cl, cf, dl, df;

            /* ROUND 1: x->y, z->w, y->x, w->z */
            k1 = div(ay, - bx);
            k4 = div(aw, - bz);
            by = by + k1 * cx;
            ay =      k1 * ax;
            dy = dy + k1 * dx;
            bw = bw + k4 * cz;
            aw =      k4 * az;
            dw = dw + k4 * dz;
            k2 = div(cx, - by);
            k5 = div(cz, - bw);
            ax = ax + k2 * ay;
            cx =      k2 * cy;
            dx = dx + k2 * dy;
            az = az + k5 * aw;
            cz =      k5 * cw;
            dz = dz + k5 * dw;

            /* ROUND 2: y->zw, z->xyw */
            k1 = div(az, - by);
            k4 = div(aw, - by);
            az =      k1 * ay;
            bz = bz + k1 * cy;
            dz = dz + k1 * dy;
            aw =      k4 * ay;
            e2 =      k4 * cy;
            dw = dw + k4 * dy;
            k2 = div(cx, - bz);
            k5 = div(cy, - bz);
            k6 = div(e2, - bz);
            ax = ax + k2 * az;
            cx =      k2 * cz;
            dx = dx + k2 * dz;
            ay = ay + k5 * az;
            cy =      k5 * cz;
            dy = dy + k5 * dz;
            aw = aw + k6 * az;
            cw = cw + k6 * cz;
            dw = dw + k6 * dz;

            pred = tid & 1;
            at0 = shfl_xor((pred == 0) ? ay : ax, 1);
            bt0 = shfl_xor((pred == 0) ? by : bx, 1);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 1);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 1);
            at1 = shfl_xor((pred == 0) ? aw : az, 1);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 1);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 1);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 1);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }

            /* ROUND 3: z->yw, y->xyzw */
            tidf = tid & (~1);
            tidl = tid | 1;
            al = shfl(az, tidl);
            bl = shfl(bz, tidl);
            cl = shfl(cz, tidl);
            dl = shfl(dz, tidl);
            k1 = div(ay, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? by : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            ay =      k1 * al;
            dy = dy + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            by = (tid == tidf) ? e1 : by;
            af = shfl(ay, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cy, tidf);
            df = shfl(dy, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            ay = ay + k3 * af;
            cy = cy + k3 * cf;
            dy = dy + k3 * df;
            az = az + k5 * af;
            cz =      k5 * cf;
            dz = dz + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 2;
            at0 = shfl_xor((pred == 0) ? az : ax, 2);
            bt0 = shfl_xor((pred == 0) ? bz : bx, 2);
            ct0 = shfl_xor((pred == 0) ? cz : cx, 2);
            dt0 = shfl_xor((pred == 0) ? dz : dx, 2);
            at1 = shfl_xor((pred == 0) ? aw : ay, 2);
            bt1 = shfl_xor((pred == 0) ? bw : by, 2);
            ct1 = shfl_xor((pred == 0) ? cw : cy, 2);
            dt1 = shfl_xor((pred == 0) ? dw : dy, 2);
            if(pred == 0) {
                az = at0, bz = bt0, cz = ct0, dz = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                ay = at1, by = bt1, cy = ct1, dy = dt1;
            }
                
            /* ROUND 4: y->zw, z->xyzw */
            tidf = tid & (~3);
            tidl = tid | 3;
            al = shfl(ay, tidl);
            bl = shfl(by, tidl);
            cl = shfl(cy, tidl);
            dl = shfl(dy, tidl);
            k1 = div(az, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? bz : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            az =      k1 * al;
            dz = dz + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            bz = (tid == tidf) ? e1 : bz;
            af = shfl(az, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cz, tidf);
            df = shfl(dz, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            az = az + k3 * af;
            cz = cz + k3 * cf;
            dz = dz + k3 * df;
            ay = ay + k5 * af;
            cy =      k5 * cf;
            dy = dy + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 4;
            at0 = shfl_xor((pred == 0) ? ay : ax, 4);
            bt0 = shfl_xor((pred == 0) ? by : bx, 4);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 4);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 4);
            at1 = shfl_xor((pred == 0) ? aw : az, 4);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 4);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 4);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 4);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }

            /* ROUND 5: z->yw, y->xyzw */
            tidf = tid & (~7);
            tidl = tid | 7;
            al = shfl(az, tidl);
            bl = shfl(bz, tidl);
            cl = shfl(cz, tidl);
            dl = shfl(dz, tidl);
            k1 = div(ay, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? by : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            ay =      k1 * al;
            dy = dy + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            by = (tid == tidf) ? e1 : by;
            af = shfl(ay, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cy, tidf);
            df = shfl(dy, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            ay = ay + k3 * af;
            cy = cy + k3 * cf;
            dy = dy + k3 * df;
            az = az + k5 * af;
            cz =      k5 * cf;
            dz = dz + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 8;
            at0 = shfl_xor((pred == 0) ? az : ax, 8);
            bt0 = shfl_xor((pred == 0) ? bz : bx, 8);
            ct0 = shfl_xor((pred == 0) ? cz : cx, 8);
            dt0 = shfl_xor((pred == 0) ? dz : dx, 8);
            at1 = shfl_xor((pred == 0) ? aw : ay, 8);
            bt1 = shfl_xor((pred == 0) ? bw : by, 8);
            ct1 = shfl_xor((pred == 0) ? cw : cy, 8);
            dt1 = shfl_xor((pred == 0) ? dw : dy, 8);
            if(pred == 0) {
                az = at0, bz = bt0, cz = ct0, dz = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                ay = at1, by = bt1, cy = ct1, dy = dt1;
            }

            pred = tid & 16;
            at0 = shfl_xor((pred == 0) ? ay : ax, 16);
            bt0 = shfl_xor((pred == 0) ? by : bx, 16);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 16);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 16);
            at1 = shfl_xor((pred == 0) ? aw : az, 16);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 16);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 16);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 16);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }


            k1 = - ax;
            e1 = ((tid == 0) ? bx : ZERO(val_t)) + k1 * last_c;
            dx = dx + k1 * last_d;
            bx = (tid == 0) ? e1 : bx;
            bf = shfl(e1, 0);
            cf = shfl(cx, 0);
            df = shfl(dx, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            cx = div(cx + k3 * cf, bx);
            dx = div(dx + k3 * df, bx);
            c_mem[i + tid] = cx;
            d_mem[i + tid] = dx;
            last_c = shfl(cx, 31);
            last_d = shfl(dx, 31);
    
            k1 = - az;
            e1 = ((tid == 0) ? bz : ZERO(val_t)) + k1 * last_c;
            dz = dz + k1 * last_d;
            bz = (tid == 0) ? e1 : bz;
            bf = shfl(e1, 0);
            cf = shfl(cz, 0);
            df = shfl(dz, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            cz = div(cz + k3 * cf, bz);
            dz = div(dz + k3 * df, bz);
            c_mem[i + (tid | 32)] = cz;
            d_mem[i + (tid | 32)] = dz;
            last_c = shfl(cz, 31);
            last_d = shfl(dz, 31);

            k1 = - ay;
            e1 = ((tid == 0) ? by : ZERO(val_t)) + k1 * last_c;
            dy = dy + k1 * last_d;
            by = (tid == 0) ? e1 : by;
            bf = shfl(e1, 0);
            cf = shfl(cy, 0);
            df = shfl(dy, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            cy = div(cy + k3 * cf, by);
            dy = div(dy + k3 * df, by);
            c_mem[i + (tid | 64)] = cy;
            d_mem[i + (tid | 64)] = dy;
            last_c = shfl(cy, 31);
            last_d = shfl(dy, 31);


            k1 = - aw;
            e1 = ((tid == 0) ? bw : ZERO(val_t)) + k1 * last_c;
            dw = dw + k1 * last_d;
            bw = (tid == 0) ? e1 : bw;
            bf = shfl(e1, 0);
            cf = shfl(cw, 0);
            df = shfl(dw, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            cw = div(cw + k3 * cf, bw);
            dw = div(dw + k3 * df, bw);
            c_mem[i + (tid | 96)] = cw;
            d_mem[i + (tid | 96)] = dw;
            last_c = shfl(cw, 31);
            last_d = shfl(dw, 31);
        }
    }
    val_t last_x = d_mem[n_eqt - WARP_SIZE];
    for(int i = n_eqt-2*WARP_SIZE; i >= 0; i -= WARP_SIZE) {
        val_t x = d_mem[i+tid] - last_x * c_mem[i+tid];
        d_mem[i+tid] = x;
        last_x = shfl(x, 0);
    }
}


template<typename val_t, int BLOCK_FUSION>
__global__ void WM6_R4_reg_kernel(val_t *a_gbl, val_t *b_gbl, val_t *c_gbl, val_t *d_gbl, int n_eqt, int n_batch) {
    const unsigned int shuffle_mask = 0xffffffff;
    const int warp_size = warpSize;
    const int tid = threadIdx.x & 31;
    const int bid = (blockIdx.x * (BLOCK_FUSION * WARP_SIZE) + threadIdx.x) >> 5;
    if(bid >= n_batch) { return; }
    val_t *a_mem = a_gbl + bid*n_eqt;
    val_t *b_mem = b_gbl + bid*n_eqt;
    val_t *c_mem = c_gbl + bid*n_eqt;
    val_t *d_mem = d_gbl + bid*n_eqt;
    
    val_t last_c = ZERO(val_t), last_d = ZERO(val_t);
    for(int i = 0; i < n_eqt; i += 4 * WARP_SIZE) {
        val_t ax, ay, az, aw, bx, by, bz, bw, cx, cy, cz, cw, dx, dy, dz, dw;
        auto a4 = load_4(a_mem + i + tid * 4);
        auto b4 = load_4(b_mem + i + tid * 4);
        auto c4 = load_4(c_mem + i + tid * 4);
        auto d4 = load_4(d_mem + i + tid * 4);
        ax = a4.x; bx = b4.x; cx = c4.x; dx = d4.x;
        ay = a4.y; by = b4.y; cy = c4.y; dy = d4.y;
        az = a4.z; bz = b4.z; cz = c4.z; dz = d4.z;
        aw = a4.w; bw = b4.w; cw = c4.w; dw = d4.w;
        {
            int tidf, tidl, pred;
            val_t k1, k2, k3, k4, k5, k6, e1, e2, at0, at1, bt0, bt1, ct0, ct1, dt0, dt1;
            val_t al, af, bl, bf, cl, cf, dl, df;

            /* ROUND 1: x->y, z->w, y->x, w->z */
            k1 = div(ay, - bx);
            k4 = div(aw, - bz);
            by = by + k1 * cx;
            ay =      k1 * ax;
            dy = dy + k1 * dx;
            bw = bw + k4 * cz;
            aw =      k4 * az;
            dw = dw + k4 * dz;
            k2 = div(cx, - by);
            k5 = div(cz, - bw);
            ax = ax + k2 * ay;
            cx =      k2 * cy;
            dx = dx + k2 * dy;
            az = az + k5 * aw;
            cz =      k5 * cw;
            dz = dz + k5 * dw;

            /* ROUND 2: y->zw, z->xyw */
            k1 = div(az, - by);
            k4 = div(aw, - by);
            az =      k1 * ay;
            bz = bz + k1 * cy;
            dz = dz + k1 * dy;
            aw =      k4 * ay;
            e2 =      k4 * cy;
            dw = dw + k4 * dy;
            k2 = div(cx, - bz);
            k5 = div(cy, - bz);
            k6 = div(e2, - bz);
            ax = ax + k2 * az;
            cx =      k2 * cz;
            dx = dx + k2 * dz;
            ay = ay + k5 * az;
            cy =      k5 * cz;
            dy = dy + k5 * dz;
            aw = aw + k6 * az;
            cw = cw + k6 * cz;
            dw = dw + k6 * dz;

            pred = tid & 1;
            at0 = shfl_xor((pred == 0) ? ay : ax, 1);
            bt0 = shfl_xor((pred == 0) ? by : bx, 1);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 1);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 1);
            at1 = shfl_xor((pred == 0) ? aw : az, 1);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 1);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 1);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 1);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }

            /* ROUND 3: z->yw, y->xyzw */
            tidf = tid & (~1);
            tidl = tid | 1;
            al = shfl(az, tidl);
            bl = shfl(bz, tidl);
            cl = shfl(cz, tidl);
            dl = shfl(dz, tidl);
            k1 = div(ay, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? by : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            ay =      k1 * al;
            dy = dy + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            by = (tid == tidf) ? e1 : by;
            af = shfl(ay, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cy, tidf);
            df = shfl(dy, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            ay = ay + k3 * af;
            cy = cy + k3 * cf;
            dy = dy + k3 * df;
            az = az + k5 * af;
            cz =      k5 * cf;
            dz = dz + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 2;
            at0 = shfl_xor((pred == 0) ? az : ax, 2);
            bt0 = shfl_xor((pred == 0) ? bz : bx, 2);
            ct0 = shfl_xor((pred == 0) ? cz : cx, 2);
            dt0 = shfl_xor((pred == 0) ? dz : dx, 2);
            at1 = shfl_xor((pred == 0) ? aw : ay, 2);
            bt1 = shfl_xor((pred == 0) ? bw : by, 2);
            ct1 = shfl_xor((pred == 0) ? cw : cy, 2);
            dt1 = shfl_xor((pred == 0) ? dw : dy, 2);
            if(pred == 0) {
                az = at0, bz = bt0, cz = ct0, dz = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                ay = at1, by = bt1, cy = ct1, dy = dt1;
            }
                
            /* ROUND 4: y->zw, z->xyzw */
            tidf = tid & (~3);
            tidl = tid | 3;
            al = shfl(ay, tidl);
            bl = shfl(by, tidl);
            cl = shfl(cy, tidl);
            dl = shfl(dy, tidl);
            k1 = div(az, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? bz : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            az =      k1 * al;
            dz = dz + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            bz = (tid == tidf) ? e1 : bz;
            af = shfl(az, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cz, tidf);
            df = shfl(dz, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            az = az + k3 * af;
            cz = cz + k3 * cf;
            dz = dz + k3 * df;
            ay = ay + k5 * af;
            cy =      k5 * cf;
            dy = dy + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 4;
            at0 = shfl_xor((pred == 0) ? ay : ax, 4);
            bt0 = shfl_xor((pred == 0) ? by : bx, 4);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 4);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 4);
            at1 = shfl_xor((pred == 0) ? aw : az, 4);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 4);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 4);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 4);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }

            /* ROUND 5: z->yw, y->xyzw */
            tidf = tid & (~7);
            tidl = tid | 7;
            al = shfl(az, tidl);
            bl = shfl(bz, tidl);
            cl = shfl(cz, tidl);
            dl = shfl(dz, tidl);
            k1 = div(ay, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? by : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            ay =      k1 * al;
            dy = dy + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            by = (tid == tidf) ? e1 : by;
            af = shfl(ay, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cy, tidf);
            df = shfl(dy, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            ay = ay + k3 * af;
            cy = cy + k3 * cf;
            dy = dy + k3 * df;
            az = az + k5 * af;
            cz =      k5 * cf;
            dz = dz + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 8;
            at0 = shfl_xor((pred == 0) ? az : ax, 8);
            bt0 = shfl_xor((pred == 0) ? bz : bx, 8);
            ct0 = shfl_xor((pred == 0) ? cz : cx, 8);
            dt0 = shfl_xor((pred == 0) ? dz : dx, 8);
            at1 = shfl_xor((pred == 0) ? aw : ay, 8);
            bt1 = shfl_xor((pred == 0) ? bw : by, 8);
            ct1 = shfl_xor((pred == 0) ? cw : cy, 8);
            dt1 = shfl_xor((pred == 0) ? dw : dy, 8);
            if(pred == 0) {
                az = at0, bz = bt0, cz = ct0, dz = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                ay = at1, by = bt1, cy = ct1, dy = dt1;
            }

            /* ROUND 6: y->zw, z->xyzw */
            tidf = tid & (~15);
            tidl = tid | 15;
            al = shfl(ay, tidl);
            bl = shfl(by, tidl);
            cl = shfl(cy, tidl);
            dl = shfl(dy, tidl);
            k1 = div(az, - bl);
            k4 = div(aw, - bl);
            e1 = ((tid == tidf) ? bz : ZERO(val_t)) + k1 * cl;
            e2 =                                      k4 * cl;
            az =      k1 * al;
            dz = dz + k1 * dl;
            aw =      k4 * al;
            dw = dw + k4 * dl;
            bz = (tid == tidf) ? e1 : bz;
            af = shfl(az, tidf);
            bf = shfl(e1, tidf);
            cf = shfl(cz, tidf);
            df = shfl(dz, tidf);
            k2 = div(cx, - bf);
            k5 = div(cy, - bf);
            k3 = maskz_div(tid == tidf, e1, - bf);
            k6 = div(e2, - bf);
            ax = ax + k2 * af;
            cx =      k2 * cf;
            dx = dx + k2 * df;
            az = az + k3 * af;
            cz = cz + k3 * cf;
            dz = dz + k3 * df;
            ay = ay + k5 * af;
            cy =      k5 * cf;
            dy = dy + k5 * df;
            aw = aw + k6 * af;
            cw = cw + k6 * cf;
            dw = dw + k6 * df;

            pred = tid & 16;
            at0 = shfl_xor((pred == 0) ? ay : ax, 16);
            bt0 = shfl_xor((pred == 0) ? by : bx, 16);
            ct0 = shfl_xor((pred == 0) ? cy : cx, 16);
            dt0 = shfl_xor((pred == 0) ? dy : dx, 16);
            at1 = shfl_xor((pred == 0) ? aw : az, 16);
            bt1 = shfl_xor((pred == 0) ? bw : bz, 16);
            ct1 = shfl_xor((pred == 0) ? cw : cz, 16);
            dt1 = shfl_xor((pred == 0) ? dw : dz, 16);
            if(pred == 0) {
                ay = at0, by = bt0, cy = ct0, dy = dt0;
                aw = at1, bw = bt1, cw = ct1, dw = dt1;
            } else {
                ax = at0, bx = bt0, cx = ct0, dx = dt0;
                az = at1, bz = bt1, cz = ct1, dz = dt1;
            }

            k1 = - ax;
            k2 = - az;
            e1 = ((tid == 0) ? bx : ZERO(val_t)) + k1 * last_c;
            e2 =                                   k2 * last_c;
            dx = dx + k1 * last_d;
            dz = dz + k2 * last_d;
            bx = (tid == 0) ? e1 : bx;
            bf = shfl(e1, 0);
            cf = shfl(cx, 0);
            df = shfl(dx, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            k4 =                 div(e2, - bf);
            cx = div(cx + k3 * cf, bx);
            dx = div(dx + k3 * df, bx);
            cz = div(cz + k4 * cf, bz);
            dz = div(dz + k4 * df, bz);
            c_mem[i + tid] = cx;
            d_mem[i + tid] = dx;
            c_mem[i + (tid | 32)] = cz;
            d_mem[i + (tid | 32)] = dz;
            last_c = shfl(cz, 31);
            last_d = shfl(dz, 31);

            k1 = - ay;
            k2 = - aw;
            e1 = ((tid == 0) ? by : ZERO(val_t)) + k1 * last_c;
            e2 =                                   k2 * last_c;
            dy = dy + k1 * last_d;
            dw = dw + k2 * last_d;
            by = (tid == 0) ? e1 : by;
            bf = shfl(e1, 0);
            cf = shfl(cy, 0);
            df = shfl(dy, 0);
            k3 = maskz_div(tid == 0, e1, - bf);
            k4 =                 div(e2, - bf);
            cy = div(cy + k3 * cf, by);
            dy = div(dy + k3 * df, by);
            cw = div(cw + k4 * cf, bw);
            dw = div(dw + k4 * df, bw);
            c_mem[i + (tid | 64)] = cy;
            d_mem[i + (tid | 64)] = dy;
            c_mem[i + (tid | 96)] = cw;
            d_mem[i + (tid | 96)] = dw;
            last_c = shfl(cw, 31);
            last_d = shfl(dw, 31);
        }
    }
    val_t last_x = - d_mem[n_eqt - 2 *WARP_SIZE];
    for(int i = n_eqt-4*WARP_SIZE; i >= 0; i -= 2*WARP_SIZE) {
        val_t x1 = d_mem[i+tid]      + last_x * c_mem[i+tid];
        val_t x2 = d_mem[i+(tid|32)] + last_x * c_mem[i+(tid|32)];
        d_mem[i+tid]      = x1;
        d_mem[i+(tid|32)] = x2;
        last_x = - shfl(x2, 0);
    }
}